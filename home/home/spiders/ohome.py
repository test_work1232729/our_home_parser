import scrapy
from home.items import HomeItem
from scrapy_playwright.page import PageMethod
from bs4 import BeautifulSoup

from urllib.parse import urljoin


class OurhomeSpider(scrapy.Spider):
    name = "ohome"
    allowed_domains = ["xn--80az8a.xn--d1aqf.xn--p1ai"]

    def start_requests(self):
        yield scrapy.Request(
            "https://xn--80az8a.xn--d1aqf.xn--p1ai/%D1%81%D0%B5%D1%80%D0%B2%D0%B8%D1%81%D1%8B/%D0%BA%D0%B0%D1%82%D0%B0%D0%BB%D0%BE%D0%B3-%D0%BD%D0%BE%D0%B2%D0%BE%D1%81%D1%82%D1%80%D0%BE%D0%B5%D0%BA/%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA-%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82%D0%BE%D0%B2/%D1%81%D0%BF%D0%B8%D1%81%D0%BE%D0%BA?place=0-44&objStatus=0",
            meta={
                "playwright": True,
                "playwright_include_page": True,
                "playwright_page_methods": [
                    PageMethod("evaluate", "window.scrollBy(0, 2000)"),
                ],
            },
            errback=self.errback,
        )

    async def parse(self, response):
        page = response.meta["playwright_page"]
        links = set()
        base_url = "https://xn--80az8a.xn--d1aqf.xn--p1ai"

        while True:
            await page.evaluate("window.scrollBy(0, document.body.scrollHeight)")
            await page.wait_for_timeout(2000)

            new_links = await page.query_selector_all(
                "a.NewBuildingItem__ImageWrapper-sc-o36w9y-2"
            )
            for link in new_links:
                href = await link.get_attribute("href")
                if href and href not in links:
                    full_url = urljoin(base_url, href)
                    links.add(full_url)

            next_page_button = await page.query_selector(
                "button.Newbuildings__ButtonLoadMore-sc-1bou0u4-15"
            )
            if next_page_button:
                await next_page_button.click()
                await page.wait_for_timeout(2000)
            else:
                break

        await page.close()

        for link in links:
            yield scrapy.Request(
                link,
                callback=self.parse_item,
                meta={
                    "playwright": True,
                    "playwright_include_page": True,
                },
            )

    async def parse_item(self, response, **kwargs):
        page = response.meta["playwright_page"]
        item = HomeItem()
        content = await page.content()
        sel = scrapy.Selector(text=content)

        item["Заголовок объявления"] = sel.css("h1::text").get().strip()
        item["ID объявления"] = response.request.url.split("/")[-1]
    

        soup = BeautifulSoup(str(sel.css("*").get()), "html.parser")

        elements = soup.find_all("p", class_="Header__Address-sc-eng632-8")
        for element in elements:
            if "Адрес:" in element.get_text():
                full_text = element.get_text(strip=True)
                break
        if full_text:
            full_text = full_text.replace("Адрес:", "").strip()
        item["Адрес"] = full_text

        data = {}

        rows = soup.find_all("div", class_="Row-sc-13pfgqd-0")

        for row in rows:
            name = row.find("div", class_="Row__Name-sc-13pfgqd-1").text.strip()
            value = row.find("div", class_="Row__Value-sc-13pfgqd-2")

            if name == "Застройщик" or name == "Группа компаний":
                value = value.find("a").text.strip()
            elif name == "Проектная декларация":
                decl_text = value.text.strip()
                decl_number = decl_text.split()[1]
                decl_date = " ".join(decl_text.split()[3:])
                value = f"{decl_number} от {decl_date}"
            else:
                value = value.text.strip()

            data[name] = value

        rows = soup.find_all("div", class_="CharacteristicsBlock__Row-sc-1fyyfia-3")
        for row in rows:
            spans = row.find_all(
                "span", class_="CharacteristicsBlock__RowSpan-sc-1fyyfia-4"
            )
            if len(spans) == 2:
                key = spans[0].text.strip()
                value = spans[1].text.strip()

                if key == "Тип отделки":
                    value = (
                        spans[1].find("a").text.strip() if spans[1].find("a") else value
                    )
                data[key] = value

        item["Ввод в эксплуатацию"] = data.get("Ввод в эксплуатацию", "")
        item["Застройщик"] = data.get("Застройщик", "")
        item["Группа компаний"] = data.get("Группа компаний", "")
        item["Дата публикации проекта"] = data.get("Дата публикации проекта", "")
        item["Выдача ключей"] = data.get("Выдача ключей", "")
        item["Средняя цена за кв.м."] = data.get("Средняя цена за 1 м²", "")
        item["Распроданность квартир"] = data.get("Распроданность квартир", "")
        item["Класс недвижимости"] = data.get("Класс недвижимости", "")
        item["Количество квартир"] = data.get("Количество квартир", "")

        yield item

        await page.close()

    async def errback(self, failure):
        page = failure.request.meta["playwright_page"]
        print("failure")
        await page.close()
