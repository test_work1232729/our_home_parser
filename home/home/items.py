# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class HomeItem(scrapy.Item):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["Заголовок объявления"] = scrapy.Field()
        self.fields["Адрес"] = scrapy.Field()
        self.fields["ID объявления"] = scrapy.Field()
        self.fields["Ввод в эксплуатацию"] = scrapy.Field()
        self.fields["Застройщик"] = scrapy.Field()
        self.fields["Группа компаний"] = scrapy.Field()
        self.fields["Дата публикации проекта"] = scrapy.Field()
        self.fields["Выдача ключей"] = scrapy.Field()
        self.fields["Средняя цена за кв.м."] = scrapy.Field()
        self.fields["Распроданность квартир"] = scrapy.Field()
        self.fields["Класс недвижимости"] = scrapy.Field()
        self.fields["Количество квартир"] = scrapy.Field()
